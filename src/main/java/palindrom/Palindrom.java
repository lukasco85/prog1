package palindrom;

import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj słowo a sprawdzę czy jest Palidnromem");

        String wordToCheck = scanner.nextLine();
        boolean isPalidrom = true;
        int start = 0;
        int end = wordToCheck.length() - 1;

        while (start <= end) {
            System.out.println("Checking equality against ->> " + wordToCheck.charAt(start) +
                                 " vs ->> " + wordToCheck.charAt(end));
            if (wordToCheck.charAt(start) != wordToCheck.charAt(end)){
                 isPalidrom = false;
                 break;
            }
            start++;
            end--;
        }
        if (isPalidrom){
            System.out.println("Podane słowo " + wordToCheck + " jest Palindromem");
        } else {
            System.out.println("Słowo " + wordToCheck + " nie jest Palindromem");
        }
    }
}
