package factorial;

import org.w3c.dom.ls.LSOutput;

public class Factorial {

    public static void main(String[] args) {
        System.out.println(factorial(5));
    }

    static int factorial(int n){
        if (n >= 1){
            return n * factorial(n - 1);
        } else {
            return 1;
        }
    }




}
